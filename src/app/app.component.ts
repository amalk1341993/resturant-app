import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'SigninPage';

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();


  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  home() {
this.nav.setRoot('HomePage');
}

  newbooking() {
this.nav.setRoot('NewbookingPage');
}

  activeorder() {
this.nav.setRoot('ActiveorderPage');
}

  buzzer() {
this.nav.setRoot('BuzzerPage');
}

  notification() {
this.nav.setRoot('NotificationPage');
}

  cookie() {
this.nav.setRoot('CookiePage');
}

  privacy() {
this.nav.setRoot('PrivacyPage');
}

  terms() {
this.nav.setRoot('TermsPage');
}

  logout() {
this.nav.setRoot('SigninPage');
}

}

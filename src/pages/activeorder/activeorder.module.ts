import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActiveorderPage } from './activeorder';

@NgModule({
  declarations: [
    ActiveorderPage,
  ],
  imports: [
    IonicPageModule.forChild(ActiveorderPage),
  ],
})
export class ActiveorderPageModule {}

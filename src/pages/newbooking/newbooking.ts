import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-newbooking',
  templateUrl: 'newbooking.html',
})
export class NewbookingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewbookingPage');
  }

  
  /* COMMON-NAVIGATION-CONTROL-FUNCTION */

  open_page(page){
  this.navCtrl.push(page);
  }

   goBack(){
    this.navCtrl.pop();
  }

}

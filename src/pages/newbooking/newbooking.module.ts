import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewbookingPage } from './newbooking';

@NgModule({
  declarations: [
    NewbookingPage,
  ],
  imports: [
    IonicPageModule.forChild(NewbookingPage),
  ],
})
export class NewbookingPageModule {}

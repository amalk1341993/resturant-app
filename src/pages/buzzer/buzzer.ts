import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-buzzer',
  templateUrl: 'buzzer.html',
})
export class BuzzerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuzzerPage');
  }

  /* COMMON-NAVIGATION-CONTROL-FUNCTION */

  open_page(page){
  this.navCtrl.push(page);
  }

   goBack(){
    this.navCtrl.pop();
  }

}

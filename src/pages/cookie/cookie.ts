import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-cookie',
  templateUrl: 'cookie.html',
})
export class CookiePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CookiePage');
  }

    /* COMMON-NAVIGATION-CONTROL-FUNCTION */

  open_page(page){
  this.navCtrl.push(page);
  }

   goBack(){
    this.navCtrl.pop();
  }

}
